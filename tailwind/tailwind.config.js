module.exports = {
  theme: {
    fontFamily: {
      display: ['Fira Sans', 'sans-serif'],
      body: ['Fira Sans', 'sans-serif'],
    },
    // this overwrites the colors, so we keep only this one, if we wanted to keep the
    // default ones like bg-blue-500, we would put them in extend
    colors: {
      primary: {
        default: "#7655E9",
        light: "#B1A0EE",
        dark: "#5039a0",
        text: "#ffffff",
      },
      cardback: "#ffffff",
      secondary: {
        default: "#fed500",
      },
      grey: "#ddddd",
      info: "#05a1ff",
    },
    extend: {},
  },
  variants: {},
  plugins: [],
}
